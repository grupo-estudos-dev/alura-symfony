<?php

namespace App\Controller;

use App\Entity\Especialidade;
use App\Controller\BaseController;
use App\Helper\EspecialidadeFactory;
use App\Helper\ExtratorDadosRequest;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\EspecialidadeRepository;

class EspecialidadesController extends BaseController
{

    public function __construct(
        EspecialidadeRepository $repository,
        EntityManagerInterface $entityManager,
        EspecialidadeFactory $factory,
        ExtratorDadosRequest $extratorDadosRequest
        )
    {
        parent::__construct($repository, $entityManager, $factory, $extratorDadosRequest);
    }

    /**
     * @param Especialidade $entidadeExistente
     * @param Especialidade $entidadeEnviada
     */
    public function atualizaEntidadeExistente($entidadeExistente, $entidadeEnviada)
    {
        $entidadeExistente->setDescricao($entidadeEnviada->getDescricao());
    }
}
